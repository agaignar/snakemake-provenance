# Singleton/SingletonPattern.py

import datetime
import uuid
import getpass
import os
import hashlib
import validators

from snakemake.logging import logger


class PROVMgr(object):
    __instance = None
    __is_active = False

    whoami = getpass.getuser()

    def __new__(self):
        if PROVMgr.__instance is None:
            PROVMgr.__instance = object.__new__(self)
        return PROVMgr.__instance

    def __init__(self):
        self.__prov = []
        self.__prov.append("@base <http://snakemake-provenance> .\n"
                           "@prefix xsd:  <http://www.w3.org/2001/XMLSchema#> .\n"
                           "@prefix foaf: <http://xmlns.com/foaf/0.1/> .\n"
                           "@prefix sioc: <http://rdfs.org/sioc/ns#> .\n"
                           "@prefix prov: <http://www.w3.org/ns/prov#> .\n"
                           "@prefix sym:   <http://fr.symetric/vocab#> .\n"
                           "@prefix crypto: <http://id.loc.gov/vocabulary/preservation/cryptographicHashFunctions#> .\n"
                           "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
                           "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n"
                           "\n"
                           "<:"+self.whoami+"> a prov:Agent, prov:Person . \n\n"
                           "<> \n"
                           "\ta prov:Bundle, prov:Entity;\n"
                           "\tprov:wasAttributedTo <:"+self.whoami+">;\n"
                           "\tprov:generatedAtTime \"" + self.__get_xsd_now + ".\n\n")

    def set_activate(self, is_active):
        self.__is_active = is_active

    def is_active(self):
        return self.__is_active

    def add_activity(self, tool_name, job_uri, input_id_list, cmd):

        logger.info("Adding PROV activity at time "+str(self.__get_xsd_now))

        associated_with = ""
        if validators.url(tool_name):
            associated_with = '\tprov:wasAssociatedWith <' + str(tool_name) + '> ;\n'
        else :
            associated_with = '\tprov:wasAssociatedWith <#' + str(tool_name) + '> ;\n'

        if cmd:
            prov_activity = "<" + str(job_uri) + ">\n" \
            + "\ta prov:Activity ;\n" \
            + '\trdfs:comment """\n\t\t' + str(cmd) + '\n\t""" ;\n' \
            + associated_with \
            + "\tprov:startedAtTime \"" + str(self.__get_xsd_now) + ";\n" \
            + "\tprov:endedAtTime \"" + str(self.__get_xsd_now) + "; \n"
        else:
            prov_activity = "<" + str(job_uri) + ">\n" \
            + "\ta prov:Activity ;\n" \
            + associated_with \
            + "\tprov:startedAtTime \"" + str(self.__get_xsd_now) + ";\n" \
            + "\tprov:endedAtTime \"" + str(self.__get_xsd_now) + "; \n"

        for in_id in input_id_list:
            prov_activity += "\tprov:used <" + in_id + "> ; \n"

        prov_activity += ".\n\n"
        self.__prov.append(prov_activity)

    def add_output(self, output_id, input_id_list, tool_name, job_uri):

        associated_with = ""
        if validators.url(tool_name):
            associated_with = '\tprov:wasAssociatedWith <' + str(tool_name) + '> ;\n'
        else:
            associated_with = '\tprov:wasAssociatedWith <#' + str(tool_name) + '> ;\n'

        output = "<" + output_id + ">\n" \
                 + "\ta prov:Entity;\n" \
                 + "\tprov:wasGeneratedBy <" + str(job_uri) + ">;\n" \
                 + associated_with \
                 + "\trdfs:label \"" + output_id + "\"; \n"

        if os.path.isfile(os.path.abspath(output_id)):
            digest = hashlib.sha512(open(os.path.abspath(output_id), 'rb').read()).hexdigest()
            output = output  + "    crypto:sha512 \""+digest+"\"^^xsd:string ; \n"

        for in_id in input_id_list:
            output += "\tprov:wasDerivedFrom <" + in_id + "> ; \n"

        output += ".\n\n"
        self.__prov.append(output)

    def print_string(self):
        print("".join(self.__prov))

    def write_to_file(self):
        prov_file = 'provenance.ttl'
        print('Writing provenance to '+str(os.getcwd()))
        with open("provenance.ttl", "w") as text_file:
            text_file.write("".join(self.__prov))

    @property
    def __get_xsd_now(self):
        return datetime.datetime.now().isoformat() + "\"^^xsd:dateTime"

    def gen_URI(self, prefix, name):
        #return uuid.uui5("http://truc", "machin")
        return prefix+name+"-"+uuid.uuid4().__str__()


############ Singleton class instanciation ###########
pMgr = PROVMgr()